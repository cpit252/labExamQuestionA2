package edu.kau.fcit.cpit252;

import io.ipinfo.api.IPinfo;
import io.ipinfo.api.errors.RateLimitedException;
import io.ipinfo.api.model.IPResponse;

import java.net.UnknownHostException;


public class App 
{
    private static String getApiKey() throws MissingRequiredPropetiesException{
        final String apikey = System.getenv("key");
        if (apikey == null || apikey == "") {
            throw new MissingRequiredPropetiesException("Missing API key. You will need to go to https://ipinfo.io, sign up for a free API key, and store the" +
                    " API key in an environment variable named: \"key\"."  +
                    " Please refer to the following link for more information on how to set environment variables:" +
                    " https://cpit252.gitlab.io/miscellaneous/#environment-variables");
        }
        return apikey;
    }
    static class MissingRequiredPropetiesException extends Exception{
        public MissingRequiredPropetiesException(String message){
            super(message);
        }
    }
    public static void main( String[] args )
    {
        try {
            final String apiKey = getApiKey();
            IPinfo ipInfo = new IPinfo.Builder()
                    .setToken(apiKey)
                    .build();

            String host = "kau.edu.sa";
            // convert the host to an IP address
            String ip = DNSProvider.hostToIPV4(host);
            // Look up the ip info (country, city, timezone, etc.)
            IPResponse response = ipInfo.lookupIP(ip);
            // Return the response in JSON
            String jsonResponse = JsonParser.serializeIPResponse(response);
            System.out.println("Host: " + host);
            System.out.println(jsonResponse);
            // convert the json response into an Object
            IPDetails ipDetails = JsonParser.parseIPInfoResponse(jsonResponse);
            System.out.println(ipDetails);
            System.exit(0);
        }
        catch (MissingRequiredPropetiesException ex){
            System.err.println(ex.getMessage());
        }
        catch (RateLimitedException ex) {
        System.err.println("Oops you have reached the API rate limit!");
        }
        catch (UnknownHostException e) {
            System.err.println("Unknown host error: " + e.getMessage());
        }
    }
}
