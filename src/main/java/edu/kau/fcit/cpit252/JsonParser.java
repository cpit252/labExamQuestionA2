package edu.kau.fcit.cpit252;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.ipinfo.api.model.IPResponse;

public class JsonParser {

    // JSON Serialization in Jackson
    public static String serializeIPResponse(IPResponse ipResponse) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(ipResponse);
            return jsonString;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    // JSON Deserialization in Jackson
    public static IPDetails parseIPInfoResponse(String jsonString){
        try {
            //  unmarshal the JSON String to Java class
            ObjectMapper mapper= new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            IPDetails ipDetails = mapper.readValue(jsonString, IPDetails.class);
            return ipDetails;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

}
