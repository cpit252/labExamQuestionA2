package edu.kau.fcit.cpit252;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DNSProvider {

    public static String hostToIPV4(String host) throws UnknownHostException {
        InetAddress inetAddress = java.net.InetAddress.getByName(host);
        return inetAddress.getHostAddress();
    }
}
