package edu.kau.fcit.cpit252;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.net.UnknownHostException;

public class DNSProviderTest {

    @Test
    public void shouldConvertHostToIPV4() throws UnknownHostException {
        String ipV4 = DNSProvider.hostToIPV4("kau.edu.sa");
        assertEquals(ipV4, "192.162.72.233");
    }
}