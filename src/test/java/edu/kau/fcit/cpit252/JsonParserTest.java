package edu.kau.fcit.cpit252;

import org.junit.Test;

import static org.junit.Assert.*;

public class JsonParserTest {

    @Test
    public void shouldDeserializeJSON(){
        String jsonString = "{\"ip\": \"192.162.72.233\",   \"hostname\": \"kau.edu.sa\",   \"city\": \"Jeddah\",   \"region\": \"Mecca Region\",   \"country\": \"SA\",   \"loc\": \"21.4901,39.1862\",   \"org\": \"AS15505 King Abdulaziz University (KAU) - Deanship of Information Technology\",   \"timezone\": \"Asia/Riyadh\" }";
        IPDetails actualIpDetailsObj = JsonParser.parseIPInfoResponse(jsonString);
        assertNotNull(actualIpDetailsObj);

        IPDetails expectedIpDetailsObj = new IPDetails();
        expectedIpDetailsObj.setIp("192.162.72.233");
        expectedIpDetailsObj.setHostname("kau.edu.sa");
        expectedIpDetailsObj.setCity("Jeddah");
        expectedIpDetailsObj.setRegion("Mecca Region");
        expectedIpDetailsObj.setCountry("SA");
        expectedIpDetailsObj.setLoc("21.4901,39.1862");
        expectedIpDetailsObj.setOrg("AS15505 King Abdulaziz University (KAU) - Deanship of Information Technology");
        expectedIpDetailsObj.setTimezone("Asia/Riyadh");

        assertEquals(expectedIpDetailsObj.toString(), actualIpDetailsObj.toString());

    }
}