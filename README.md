# Lab Exam - Question A2

## Description

A developer has created a service that returns information for a given domain (e.g., kau.edu.sa). The developer created a service that takes a host or a domain name and returns the city, country, and time zone for that domain. He found a library that takes an IP address, performs a lookup, and returns the related details for the IP address (city, country, and time zone). However, the developer wanted to interact with the library using the host name not the IP address. He needs to write an adapter to convert the interface of this library into another interface his code expects. 




## Requirement
Go to [https://ipinfo.io/](https://ipinfo.io/) and sign up for a free account to obtain API keys. Add the API key as an environemtn variable called `key`.

You will need to go to https://ipinfo.io, sign up for a free API key, and store the API key in an environment variable named: `key`. Please refer to the following link for more information on how to set environment variables: [https://cpit252.gitlab.io/miscellaneous/#environment-variables](https://cpit252.gitlab.io/miscellaneous/#environment-variables).


## Question
Refactor the code using the adapter design pattern.

